const express = require('express'),
     http = require('http');
const morgan = require('morgan');
const bodyParser = require('body-parser');

const dishRouter = require('./routes/dishRouter');
const dishIdRouter = require('./routes/dishIdRouter');

const promoRouter = require('./routes/promoRouter');
const promoIdRouter = require('./routes/promoIdRouter');

const leaderRouter = require('./routes/leaderRouter');
const leaderIdRouter = require('./routes/leaderIdRouter');
const hostname = 'localhost';
const port = 3000;

const app = express();
app.use(morgan('dev'));
app.use(bodyParser.json());

app.use('/dishes', dishRouter);
app.use('/dishes/:dishId',  dishIdRouter);
app.use('/promotions', promoRouter);
app.use('/promotions/:promoId',  promoIdRouter);
app.use('/leaders', leaderRouter);
app.use('/leaders/:leaderId',  leaderIdRouter);
app.use(express.static(__dirname + '/public'));


app.use((req, res, next) => {
  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/html');
  res.end('<html><body><h1>This is an Express Server</h1></body></html>');

});

const server = http.createServer(app);

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});