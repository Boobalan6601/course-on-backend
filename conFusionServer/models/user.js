const { Schema } = require('mongoose');
var mongoose = require('mongoose');
var schema = mongoose.schema;
var passportLocalMongoose = require('passport-local-mongoose');

var User = new Schema({
    firstname: {
        type: String,
        default: ''
    },
    lastname:{
        type: String,
        default: ''
    },
    admin:   {
        type: Boolean,
        default: false
    }
});

User.plugin(passportLocalMongoose);

module.exports= mongoose.model('User' , User);